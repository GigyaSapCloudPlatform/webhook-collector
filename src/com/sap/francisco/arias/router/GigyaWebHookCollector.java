package com.sap.francisco.arias.router;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;

import com.gigya.socialize.*;

/**
 * Servlet implementation class GigyaWebHookCollector
 */
@WebServlet("/")
public class GigyaWebHookCollector extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(GigyaWebHookCollector.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GigyaWebHookCollector() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Gigya WebHook Collector from SAP Cloud Platform");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			doAdd(request, response);
			response.setStatus(200);

		} catch (Exception e) {
			response.getWriter().println("Persistence operation failed with reason: " + e.getMessage());
			LOGGER.error("Persistence operation failed", e);
		}
	}

	private void doAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Extract name of person to be added from request
		String method = request.getMethod();
		String header = "";
		String parameter = "";
		String body = "-";

		String gigya_sig = "";

		// HEADERS
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			String headerValue = request.getHeader(headerName);

			header += headerName + ":" + headerValue + "\n";

			// Save the Gigya signature
			if (headerName.equals("x-gigya-sig-hmac-sha1")) {
				gigya_sig = headerValue;
			}
		}

		// BODY
		InputStream is = request.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte buf[] = new byte[1024];
		int letti;

		while ((letti = is.read(buf)) > 0)
			baos.write(buf, 0, letti);

		body = new String(baos.toByteArray());
		// response.getWriter().println("Body: " + body + "\n");

		// PARAMETERS
		Map<String, String[]> params = request.getParameterMap();
		for (Iterator<String> it = params.keySet().iterator(); it.hasNext();) {

			String k = it.next();
			parameter += k + ":";

			String[] v = params.get(k);
			if (v != null && v.length > 0) {
				for (int i = 0; i < v.length; i++) {
					parameter += v[i] + ",";
				}
			}

			parameter += "\n";
		}

		// Validation
		// ================================

		byte[] secret_x64 = Base64.getDecoder().decode(com.sap.francisco.arias.config.gigya.keysecret().getBytes());

		Mac mac = Mac.getInstance("HmacSHA1");
		SecretKeySpec secret = new SecretKeySpec(secret_x64, "HmacSHA1");
		mac.init(secret);
		byte[] digest = mac.doFinal(body.getBytes());

		byte[] message_hash = Base64.getEncoder().encode(digest);
		String message_hast_sig = new String(message_hash);

		response.getWriter().println("--------------------");
		response.getWriter().println("GIGYA SIG: " + gigya_sig);
		response.getWriter().println("MESSG SIG: " + message_hast_sig);
		response.getWriter().println("--------------------");

		if (!gigya_sig.equals(message_hast_sig))
			throw new Exception("FRAUD");

		// EVENTS
		// ================================

		try {

			// WEBHOOK
			// ================================

			// Convert the string in JSON object
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(body);
			JSONObject jsonObject = (JSONObject) obj;

			// General information
			String nonce = (String) jsonObject.get("nonce");
			response.getWriter().println("Nonce: " + nonce);

			// loop event object
			JSONArray events = (JSONArray) jsonObject.get("events");
			for (Object e : events) {

				JSONObject ev = (JSONObject) e;

				// Info
				String id = (String) ev.get("id");
				String type = (String) ev.get("type");
				Long timestamp = (Long) jsonObject.get("timestamp");

				JSONObject data = (JSONObject) ev.get("data");
				String uid = (String) data.get("uid");

				// DEBUG
				response.getWriter().println("--------------------");
				response.getWriter().println("ID: " + id);
				response.getWriter().println("Type: " + type);
				response.getWriter().println("UID: " + uid);
				response.getWriter().println(timestamp.toString());

			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
